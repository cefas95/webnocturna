﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webMatriculacionVehicular.Startup))]
namespace webMatriculacionVehicular
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
