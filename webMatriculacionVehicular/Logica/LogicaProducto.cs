﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using webMatriculacionVehicular.Models;

namespace webMatriculacionVehicular.Logica
{

    public class LogicaProducto
    {
        private static BDDCORDICARRITOEntities db = new BDDCORDICARRITOEntities();
        public static async Task<List<TBL_PRODUCTO>> getAllProduct()
        {
            try
            {
                return await db.TBL_PRODUCTO.Where(data => data.pro_status.Equals("A")).
                       ToListAsync();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener productos");
            }

        }

        public static async Task<TBL_PRODUCTO> getProductbyId(int idProducto)
        {
            try
            {
                return await db.TBL_PRODUCTO.Where(data => data.pro_status.Equals("A")
                                                   && data.pro_id.Equals(idProducto)).
                       FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw new ArgumentException("Error al obtener productos");
            }

        }

        public static async Task<bool> saveProducto(TBL_PRODUCTO producto)
        {
            try
            {
                bool result = false;

                producto.pro_status = "A";
                producto.pro_fechacreacion = DateTime.Now;
                db.TBL_PRODUCTO.Add(producto);

                //commit hacia la base de datos
                await db.SaveChangesAsync();
                result = true;
                return result;

            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al guardar productos");
            }

        }

        public static async Task<bool> saveProducto2(TBL_PRODUCTO producto)
        {
            try
            {
                bool result = false;

                producto.pro_status = "A";
                producto.pro_fechacreacion = DateTime.Now;
                
                // Asyncrono
                //db.stpInsertProduct(producto.pro_codigo,)

                //commit hacia la base de datos
                await db.SaveChangesAsync();
                result = true;
                return result;

            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al guardar productos");
            }

        }


        public static async Task<bool> updateProducto(TBL_PRODUCTO producto)
        {
            try
            {
                //bool result = false;

                producto.pro_fechacreacion = DateTime.Now;                

                //commit hacia la base de datos
                return await db.SaveChangesAsync() > 0;
                //result = true;
                //return result;

            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al guardar productos");
            }

        }

        public static async Task<bool> deleteProducto(TBL_PRODUCTO producto)
        {
            try
            {
                //bool result = false;

                //Logica
                producto.pro_fechacreacion = DateTime.Now;
                producto.pro_status = "I";

                //fisicamente
                //db.TBL_PRODUCTO.Remove(producto);

                //commit hacia la base de datos
                return await db.SaveChangesAsync() > 0;
                //result = true;
                //return result;

            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al guardar productos");
            }

        }





    }
}